INavFile nav{ };

bool Load()
{
    std::ifstream ifs("/path/to/map.nav", std::ios::binary);

	if (!ifs.is_open())
	{
		return false;
	}

	if (!nav.Load(ifs))
	{
		return false;
	}

	ifs.close();
	
	return true;
}

// Draws the navigation mesh
void DrawMesh(void)
{
	for (size_t s{ 0 }; s < nav.m_areas.size(); ++s)
	{
		const CNavArea *pArea{ &nav.m_areas[s] };

		Vector a{ pArea->m_nwCorner };
		Vector b{ pArea->m_seCorner.x, pArea->m_nwCorner.z, pArea->m_neY };
		Vector c{ pArea->m_seCorner };
		Vector d{ pArea->m_nwCorner.x, pArea->m_seCorner.z, pArea->m_swY };

		debugoverlay->AddLineOverlay(a, b, 255, 0, 0, true, 0.1f);
		debugoverlay->AddLineOverlay(b, c, 255, 0, 0, true, 0.1f);
		debugoverlay->AddLineOverlay(c, d, 255, 0, 0, true, 0.1f);
		debugoverlay->AddLineOverlay(d, a, 255, 0, 0, true, 0.1f);
	}
}